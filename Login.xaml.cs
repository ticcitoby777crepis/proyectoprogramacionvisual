﻿using ProyectoBB.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProyectoBB
{
    /// <summary>
    /// Lógica de interacción para Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        public Login()
        {
            InitializeComponent();
        }

        private void Entrar_Click(object sender, RoutedEventArgs e)
        {
            agenda23amsamgContext conexion = new agenda23amsamgContext();//abrir conexion

            //buscar usuario
            Usuario? usuario = conexion.Usuarios
            .Where(u => u.Nombre == txtNombre.Text.ToString() && u.Password == txtPassword.Password.ToString())
            .FirstOrDefault<Usuario>();

            conexion.Dispose();//cerrar conexion

            if(usuario != null)
            {
                new WindowNw().Show();//Mostrar otra vista
                Close();//Cerrar login 
            }
            else
            {
                MessageBox.Show("No entras");//no existe
            }
        }
    }
}
