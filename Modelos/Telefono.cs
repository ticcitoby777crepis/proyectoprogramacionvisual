﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProyectoBB.Modelos
{
    public partial class Telefono
    {
        public int Id { get; set; }
        public string Lada { get; set; }
        public string Numero { get; set; }
        public int ContactosId { get; set; }
        public int TiposId { get; set; }

        public virtual Contacto Contactos { get; set; }
        public virtual Tipo Tipos { get; set; }
    }
}
