﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProyectoBB.Modelos
{
    public partial class Tipo
    {
        public Tipo()
        {
            Correos = new HashSet<Correo>();
            Telefonos = new HashSet<Telefono>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }

        public virtual ICollection<Correo> Correos { get; set; }
        public virtual ICollection<Telefono> Telefonos { get; set; }
    }
}
