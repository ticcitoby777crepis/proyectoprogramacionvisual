﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProyectoBB.Modelos
{
    public partial class Usuario
    {
        public Usuario()
        {
            Contactos = new HashSet<Contacto>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Correo { get; set; }
        public string Password { get; set; }

        public virtual ICollection<Contacto> Contactos { get; set; }
    }
}
