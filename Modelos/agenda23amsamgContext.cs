﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace ProyectoBB.Modelos
{
    public partial class agenda23amsamgContext : DbContext
    {
        public agenda23amsamgContext()
        {
        }

        public agenda23amsamgContext(DbContextOptions<agenda23amsamgContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Contacto> Contactos { get; set; }
        public virtual DbSet<Correo> Correos { get; set; }
        public virtual DbSet<Direccione> Direcciones { get; set; }
        public virtual DbSet<Telefono> Telefonos { get; set; }
        public virtual DbSet<Tipo> Tipos { get; set; }
        public virtual DbSet<Usuario> Usuarios { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseMySQL("server=localhost;port=3306;user=root;password=;database=agenda(23amsamg)");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Contacto>(entity =>
            {
                entity.ToTable("contactos");

                entity.HasIndex(e => e.DireccionesId, "fk_Contactos_Direcciones1_idx");

                entity.HasIndex(e => e.UsuarioId, "fk_Contactos_Usuario_idx");

                entity.HasIndex(e => e.Telefonos, "telefonos_UNIQUE")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnType("int(10) unsigned")
                    .HasColumnName("id");

                entity.Property(e => e.ApellidoM)
                    .IsRequired()
                    .HasMaxLength(45)
                    .HasColumnName("apellido_m");

                entity.Property(e => e.ApellidoP)
                    .IsRequired()
                    .HasMaxLength(45)
                    .HasColumnName("apellido_p");

                entity.Property(e => e.DireccionesId)
                    .HasColumnType("int(11)")
                    .HasColumnName("Direcciones_id");

                entity.Property(e => e.Nombre1)
                    .IsRequired()
                    .HasMaxLength(45)
                    .HasColumnName("nombre_1");

                entity.Property(e => e.Nombre2)
                    .IsRequired()
                    .HasMaxLength(45)
                    .HasColumnName("nombre_2");

                entity.Property(e => e.Telefonos)
                    .IsRequired()
                    .HasMaxLength(10)
                    .HasColumnName("telefonos");

                entity.Property(e => e.UsuarioId)
                    .HasColumnType("int(10) unsigned")
                    .HasColumnName("Usuario_id");

                entity.HasOne(d => d.Direcciones)
                    .WithMany(p => p.Contactos)
                    .HasForeignKey(d => d.DireccionesId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_Contactos_Direcciones1");

                entity.HasOne(d => d.Usuario)
                    .WithMany(p => p.Contactos)
                    .HasForeignKey(d => d.UsuarioId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_Contactos_Usuario");
            });

            modelBuilder.Entity<Correo>(entity =>
            {
                entity.ToTable("correos");

                entity.HasIndex(e => e.Correo1, "Correo_UNIQUE")
                    .IsUnique();

                entity.HasIndex(e => e.ContactosId, "fk_Correos_Contactos1_idx");

                entity.HasIndex(e => e.TiposId, "fk_Correos_Tipos1_idx");

                entity.Property(e => e.Id)
                    .HasColumnType("int(11)")
                    .HasColumnName("id");

                entity.Property(e => e.ContactosId)
                    .HasColumnType("int(10) unsigned")
                    .HasColumnName("Contactos_id");

                entity.Property(e => e.Correo1)
                    .IsRequired()
                    .HasMaxLength(45)
                    .HasColumnName("Correo");

                entity.Property(e => e.TiposId)
                    .HasColumnType("int(11)")
                    .HasColumnName("Tipos_id");

                entity.HasOne(d => d.Contactos)
                    .WithMany(p => p.Correos)
                    .HasForeignKey(d => d.ContactosId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_Correos_Contactos1");

                entity.HasOne(d => d.Tipos)
                    .WithMany(p => p.Correos)
                    .HasForeignKey(d => d.TiposId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_Correos_Tipos1");
            });

            modelBuilder.Entity<Direccione>(entity =>
            {
                entity.ToTable("direcciones");

                entity.Property(e => e.Id)
                    .HasColumnType("int(11)")
                    .HasColumnName("id");

                entity.Property(e => e.Calle)
                    .HasMaxLength(45)
                    .HasColumnName("calle")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Colonia)
                    .HasMaxLength(45)
                    .HasColumnName("colonia")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Cp)
                    .HasMaxLength(45)
                    .HasColumnName("cp")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Estado)
                    .HasMaxLength(45)
                    .HasColumnName("estado")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Lote)
                    .HasMaxLength(45)
                    .HasColumnName("lote")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Manzana)
                    .HasMaxLength(45)
                    .HasColumnName("manzana")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Municipio)
                    .HasMaxLength(45)
                    .HasColumnName("municipio")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Pais)
                    .HasMaxLength(45)
                    .HasColumnName("pais")
                    .HasDefaultValueSql("'NULL'");
            });

            modelBuilder.Entity<Telefono>(entity =>
            {
                entity.ToTable("telefono");

                entity.HasIndex(e => e.ContactosId, "fk_telefono_Contactos1_idx");

                entity.HasIndex(e => e.TiposId, "fk_telefono_Tipos1_idx");

                entity.HasIndex(e => e.Lada, "lada_UNIQUE")
                    .IsUnique();

                entity.HasIndex(e => e.Numero, "numero_UNIQUE")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnType("int(11)")
                    .HasColumnName("id");

                entity.Property(e => e.ContactosId)
                    .HasColumnType("int(10) unsigned")
                    .HasColumnName("Contactos_id");

                entity.Property(e => e.Lada)
                    .IsRequired()
                    .HasMaxLength(45)
                    .HasColumnName("lada");

                entity.Property(e => e.Numero)
                    .IsRequired()
                    .HasMaxLength(45)
                    .HasColumnName("numero");

                entity.Property(e => e.TiposId)
                    .HasColumnType("int(11)")
                    .HasColumnName("Tipos_id");

                entity.HasOne(d => d.Contactos)
                    .WithMany(p => p.TelefonosNavigation)
                    .HasForeignKey(d => d.ContactosId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_telefono_Contactos1");

                entity.HasOne(d => d.Tipos)
                    .WithMany(p => p.Telefonos)
                    .HasForeignKey(d => d.TiposId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_telefono_Tipos1");
            });

            modelBuilder.Entity<Tipo>(entity =>
            {
                entity.ToTable("tipos");

                entity.Property(e => e.Id)
                    .HasColumnType("int(11)")
                    .HasColumnName("id");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(45)
                    .HasColumnName("nombre");
            });

            modelBuilder.Entity<Usuario>(entity =>
            {
                entity.ToTable("usuario");

                entity.HasIndex(e => e.Correo, "Correo_UNIQUE")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnType("int(10) unsigned")
                    .HasColumnName("id");

                entity.Property(e => e.Correo)
                    .IsRequired()
                    .HasMaxLength(45);

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(45);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(45)
                    .HasColumnName("password");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
