﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProyectoBB.Modelos
{
    public partial class Direccione
    {
        public Direccione()
        {
            Contactos = new HashSet<Contacto>();
        }

        public int Id { get; set; }
        public string Calle { get; set; }
        public string Manzana { get; set; }
        public string Lote { get; set; }
        public string Cp { get; set; }
        public string Municipio { get; set; }
        public string Colonia { get; set; }
        public string Estado { get; set; }
        public string Pais { get; set; }

        public virtual ICollection<Contacto> Contactos { get; set; }
    }
}
