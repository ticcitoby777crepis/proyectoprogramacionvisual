﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProyectoBB.Modelos
{
    public partial class Contacto
    {
        public Contacto()
        {
            Correos = new HashSet<Correo>();
            TelefonosNavigation = new HashSet<Telefono>();
        }

        public int Id { get; set; }
        public string Nombre1 { get; set; }
        public string Nombre2 { get; set; }
        public string ApellidoP { get; set; }
        public string ApellidoM { get; set; }
        public string Telefonos { get; set; }
        public int UsuarioId { get; set; }
        public int DireccionesId { get; set; }

        public virtual Direccione Direcciones { get; set; }
        public virtual Usuario Usuario { get; set; }
        public virtual ICollection<Correo> Correos { get; set; }
        public virtual ICollection<Telefono> TelefonosNavigation { get; set; }
    }
}
