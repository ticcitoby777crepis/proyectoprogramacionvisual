﻿using System;
using System.Collections.Generic;

#nullable disable

namespace ProyectoBB.Modelos
{
    public partial class Correo
    {
        public int Id { get; set; }
        public string Correo1 { get; set; }
        public int TiposId { get; set; }
        public int ContactosId { get; set; }

        public virtual Contacto Contactos { get; set; }
        public virtual Tipo Tipos { get; set; }
    }
}
