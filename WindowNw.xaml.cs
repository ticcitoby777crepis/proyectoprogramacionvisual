﻿using ProyectoBB.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProyectoBB
{
    /// <summary>
    /// Lógica de interacción para WindowNw.xaml
    /// </summary>
    public partial class WindowNw : Window
    {
        private List<Usuario> usuarios;
        private Usuario? usuarioEditado;
        public WindowNw()
        {
            InitializeComponent();
            CargarUsuarios();
        }
        public void CargarUsuarios()
        {
            agenda23amsamgContext conexion = new agenda23amsamgContext();
            usuarios = conexion.Usuarios.ToList();
            tblUsuarios.ItemsSource = usuarios;
            conexion.Dispose();
        }

        private void tblUsuarios_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
            {
                agenda23amsamgContext conexion = new agenda23amsamgContext();
                for (int i = 0; i < tblUsuarios.SelectedItems.Count; i++)
                {
                    Usuario? usuario = (Usuario)tblUsuarios.SelectedItems[i];
                    Usuario uTemporal = usuario;
                    conexion.Usuarios.Remove(uTemporal);
                }
                conexion.SaveChanges();
                conexion.Dispose();
            }
        }
        private void ButtonGuardar(object sender, RoutedEventArgs e)
        {
            
                Modelos.Usuario usuario = new Modelos.Usuario();

                usuario.Id = 0;
                usuario.Nombre = txtNombres.Text.ToString();
                //usuario.Apellidos = txtApellidos.Text.ToString();
                usuario.Correo = txtCorreo.Text.ToString();
                usuario.Password = Convert.ToBase64String(Encoding.UTF8.GetBytes(txtPassword.Password));
                usuarios.Add(usuario);
                tblUsuarios.Items.Refresh();

                agenda23amsamgContext conexion = new agenda23amsamgContext();
                conexion.Usuarios.Add(usuario);
                conexion.SaveChanges();
                conexion.Dispose();
            
        }
        private void tblUsuarios_PreviewMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            usuarioEditado = (Usuario)tblUsuarios.SelectedItem;
            if (usuarioEditado == null)
            {
                return;
            }
            txtNombres.Text = usuarioEditado.Nombre;
            //txtApellidos.Text = usuarioEditado.Apellidos;
            txtCorreo.Text = usuarioEditado.Correo;
        }
        private void ButtonActualizar(Object sender, RoutedEventArgs e)
        {
            if (usuarioEditado == null)
            {
                return;
            }

            Usuario usuario = usuarioEditado; //Usando el usuario seleccionado

            usuario.Nombre = txtNombres.Text.ToString();
            //usuario.Apellidos = txtApellidos.Text.ToString();
            usuario.Correo = txtCorreo.Text.ToString();
            tblUsuarios.Items.Refresh();
            // Patatas con melasa 

            agenda23amsamgContext conexion = new agenda23amsamgContext();
            conexion.Usuarios.Update(usuario);//Modo de actualizacion en bd
            conexion.SaveChanges();
            conexion.Dispose();

            usuarioEditado = null;//Eliminar edicion
        }
    }
}
